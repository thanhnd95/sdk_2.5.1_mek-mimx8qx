Overview
========


Toolchain supported
===================
- IAR embedded Workbench 8.30.2
- GCC ARM Embedded 7-2017-q4-major

Hardware requirements
=====================
- Micro USB cable
- i.MX8QM MEK CPU Board
- J-Link Debug Probe
- 12V power supply
- X-IMX-LVDS-HDMI convert board
- Monitor supports HDMI input
- MAX9286 IMX8 APPS board
- MAXCAM10635EVKIT
- OV5640 camera
- Personal Computer

There are 2 other display options to use MIPI_DSI interface:
1. replace X-IMX-LVDS-HDMI with X-IMX-MIPI-HDMI convert board.
2. Using OLED panel but not monitor, in this case X-IMX-LVDS-HDMI board and monitor
are not required, replace it using MX8_DSI_OLED1 board

There's another camera option:
OV5640 is supported with following hardware instead of MAX9286 IMX8 APPS board and MAXCAM10635EVKIT:
- OV5640 camera adapter board
- OV5640 camera

Board settings
==============
No special is needed.

Prepare the Demo
================
1.  Connect 12V power supply and J-Link Debug Probe to the board.
2.  Use X-IMX-VAL-SAS cable to connect J8 and X-IMX-LVDS-HDMI board, and connect
    the X-IMX-LVDS-HDMI convert board with the monitor.
3.  Connect MAX9286 IMX8 DEMO board J9 and MAXCAM10635EVKIT, make sure J15 on MAX9286 IMX8 DEMO board is shorten.
4.  Connect MAX9286 IMX8 DEMO board to CPU board J10 use X-IMX-VAL-SAS cable.
5.  Connect a USB cable between the host PC and the Debug port on the board (Refer "Getting Started with MCUXpresso SDK for i.MX 8QuadMax.pdf" for debug port information).
6.  Open a serial terminal with the following settings:
    - 115200 baud rate
    - 8 data bits
    - No parity
    - One stop bit
    - No flow control
7.  Build the program and combine the m4_image.bin with u-boot image using mkimage tool.
8.  Write the combined flash.bin image to SD card.
9.  Insert the SD card to MEK CPU board and power on the board.

If use OLED pannel to display, change step 2 to:
    Use X-IMX-VAL-SAS cable to connect J9 and MX8_DSI_OLED1 board. Then change
    #define APP_DISPLAY_EXTERNAL_CONVERTOR 1
    to
    #define APP_DISPLAY_EXTERNAL_CONVERTOR 0
    and
    #define DPU_EXAMPLE_DI DPU_DI_LVDS
    to
    #define DPU_EXAMPLE_DI DPU_DI_MIPI
    in isi_config.h.
If use X-IMX-MIPI-HDMI convert board, change step 2 to:
    Use X-IMX-VAL-SAS cable to connect J9 and X-IMX-MIPI-HDMI board. Then change
    #define DPU_EXAMPLE_DI DPU_DI_LVDS
    to
    #define DPU_EXAMPLE_DI DPU_DI_MIPI
    in isi_config.h.

If use OV5640 via MIPI-CSI interface, change step 3 and step 4, connect OV5640 camera to camera adapter board, and use
X-IMX-VAL-SAS cable to connect J10 port and camera adapter board. In isi_config.h, change
    #define CAMERA_DEVICE CAMERA_DEVICE_MAX9286
    to
    #define CAMERA_DEVICE CAMERA_DEVICE_OV5640

Running the demo
================
When the example runs successfully, the progress bar will be shown on the screen, and user can input "r" or
"d" to simulate automotive reverse and drive event.
If Android running on Cortex-A core hasn't started up, the reverse event will cause camera images displayed
on screen directly, and the drive event will stop the camera display and display the progress bar again.
If Android starts up, it will take over the display and camera, and further reverse or drive event will be
sent from this demo to Android application for further processing.

The log below shows the output of the rear view camera demo in the terminal window:
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Rear View Camera Demo
Simulate Auto Event:
Press 'r' for Reverse
Press 'd' for Drive
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

NOTE: Before load and run the project, press SW1 until D3/D5 are off, then press
SW1 until D3/D5 are on. This perform a whole reset.
Customization options
=====================

