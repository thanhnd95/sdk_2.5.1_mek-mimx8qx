Overview
========

The srtm_demo demo application demonstrates the use of SRTM service provided in KSDK. The demo
works together with Linux kernel, providing virtual sound card to it. Linux will utilize SRTM
APIs to configure the CODEC. The commands are transferred via RPMSG to M core, then based on
service protocols, M core will handle the configuration to CODEC hardware.

Toolchain supported
===================
- IAR embedded Workbench 8.30.2
- GCC ARM Embedded 7-2017-q4-major

Hardware requirements
=====================
- Micro USB cable
- i.MX8QX MEK Board
- MCIMX8-8X-BB
- J-Link Debug Probe
- 12V power supply
- Personal Computer
- 2 RCA to 3.5 female audio cable

Board settings
==============
Base Board is needed to run this demo.

Prepare the Demo
================
1.  Connect 12V power supply and J-Link Debug Probe to the board, switch SW3 to power on the board.
2.  Connect a micro USB cable between the host PC and the J11 USB port on the cpu board.
3.  Insert AUDIO extended card into AUDIO SLOT-1 on the base board.
4.  Using the RCA to 3.5 female cable to connect Line Out audio slots (AOUT0 and AOUT1) on the audio board and a headphone.
5.  Open a serial terminal with the following settings:
    - 115200 baud rate
    - 8 data bits
    - No parity
    - One stop bit
    - No flow control
6.  Download the program to the target board.
7.  Launch the debugger in your IDE to begin running the example.

Running the demo
================
When the example runs successfully, the following message is displayed in the terminal:

~~~~~~~~~~~~~~~~~~~~~
####################  CODEC SRTM DEMO ####################

    Build Time: Sep 12 2018--10:49:29

##########################################################
~~~~~~~~~~~~~~~~~~~~~

The background audio codec service is provided for both CS42888 and WM8960. 

Now boot up the Linux kernel with M4 specific dtb

~~~~~~~~~~~~~~~~~~~~~
setenv fdt_file 'fsl-imx8qxp-mek-m4.dtb'
save
run bootcmd
~~~~~~~~~~~~~~~~~~~~~

After entering the kernel console. Use the
following command to check CS42888 playback

~~~~~~~~~~~~~~~~~~~~~
aplay -Dplughw:0 /unit_tests/ASRC/audio8k16S.wav
~~~~~~~~~~~~~~~~~~~~~

Now Insert Headphone to J8 and use the following command to check WM8960 playback

~~~~~~~~~~~~~~~~~~~~~
aplay -Dplughw:1 /unit_tests/ASRC/audio8k16S.wav
~~~~~~~~~~~~~~~~~~~~~
Customization options
=====================

