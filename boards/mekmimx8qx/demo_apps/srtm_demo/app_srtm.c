/*
 * Copyright (c) 2016, Freescale Semiconductor, Inc.
 * Copyright 2016-2018 NXP
 * All rights reserved.
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include "FreeRTOS.h"
#include "task.h"
#include "timers.h"
#include "fsl_debug_console.h"
#include "fsl_lpi2c_freertos.h"
#include "fsl_wm8960.h"
#include "fsl_cs42888.h"

#include "srtm_dispatcher.h"
#include "srtm_peercore.h"
#include "srtm_message.h"
#include "srtm_auto_service.h"
#include "srtm_audio_service.h"
#include "srtm_i2c_codec_adapter.h"
#include "srtm_rpmsg_endpoint.h"

#include "app_srtm.h"

/*******************************************************************************
 * Definitions
 ******************************************************************************/
#define APP_MS2TICK(ms) ((ms + portTICK_PERIOD_MS - 1) / portTICK_PERIOD_MS)

typedef enum
{
    APP_SRTM_StateRun = 0x0U,
    APP_SRTM_StateLinkedUp,
    APP_SRTM_StateReboot,
    APP_SRTM_StateShutdown,
} app_srtm_state_t;

/*******************************************************************************
 * Prototypes
 ******************************************************************************/

/*******************************************************************************
 * Variables
 ******************************************************************************/
static srtm_status_t APP_SRTM_Auto_RegisterEvent(srtm_auto_adapter_t adapter,
                                                 uint32_t clientId,
                                                 uint8_t *reqData,
                                                 uint32_t reqLen,
                                                 uint8_t *respData,
                                                 uint32_t respLen);
static srtm_status_t APP_SRTM_Auto_UnregisterEvent(srtm_auto_adapter_t adapter,
                                                   uint32_t clientId,
                                                   uint8_t *reqData,
                                                   uint32_t reqLen,
                                                   uint8_t *respData,
                                                   uint32_t respLen);
static srtm_status_t APP_SRTM_Auto_Control(srtm_auto_adapter_t adapter,
                                           uint32_t clientId,
                                           uint8_t *reqData,
                                           uint32_t reqLen,
                                           uint8_t *respData,
                                           uint32_t respLen);
static srtm_status_t APP_SRTM_Auto_PowerReport(srtm_auto_adapter_t adapter,
                                               uint32_t clientId,
                                               uint8_t *reqData,
                                               uint32_t reqLen,
                                               uint8_t *respData,
                                               uint32_t respLen);
static srtm_status_t APP_SRTM_Auto_GetInfo(srtm_auto_adapter_t adapter,
                                           uint32_t clientId,
                                           uint8_t *reqData,
                                           uint32_t reqLen,
                                           uint8_t *respData,
                                           uint32_t respLen);

static lpi2c_rtos_handle_t lpi2cHandle;
static lpi2c_rtos_handle_t *codecI2cHandle;
static srtm_dispatcher_t disp;
static srtm_peercore_t core;
static srtm_service_t audioService;
static srtm_service_t autoService;
static SemaphoreHandle_t monSig;
static volatile app_srtm_state_t srtmState;
static struct rpmsg_lite_instance *rpmsgHandle;
static app_rpmsg_monitor_t rpmsgMonitor;
static void *rpmsgMonitorParam;
static TimerHandle_t linkupTimer;

static struct _srtm_auto_adapter autoAdapter = {.registerEvent = APP_SRTM_Auto_RegisterEvent,
                                                .unregisterEvent = APP_SRTM_Auto_UnregisterEvent,
                                                .control = APP_SRTM_Auto_Control,
                                                .powerReport = APP_SRTM_Auto_PowerReport,
                                                .getInfo = APP_SRTM_Auto_GetInfo};

static app_auto_register_handler_t appRegHandler;
static void *appRegParam;
/*******************************************************************************
 * Code
 ******************************************************************************/
static status_t I2C_SendFunc(lpi2c_rtos_handle_t *handle,
                             uint8_t deviceAddress,
                             uint32_t subAddress,
                             uint8_t subAddressSize,
                             const uint8_t *txBuff,
                             uint8_t txBuffSize)
{
    lpi2c_master_transfer_t masterXfer;

    /* Prepare transfer structure. */
    masterXfer.slaveAddress = deviceAddress;
    masterXfer.direction = kLPI2C_Write;
    masterXfer.subaddress = subAddress;
    masterXfer.subaddressSize = subAddressSize;
    masterXfer.data = (void *)txBuff;
    masterXfer.dataSize = txBuffSize;
    masterXfer.flags = kLPI2C_TransferDefaultFlag;

    /* Calling I2C Transfer API to start send. */
    return LPI2C_RTOS_Transfer(handle, &masterXfer);
}

static status_t I2C_ReceiveFunc(lpi2c_rtos_handle_t *handle,
                                uint8_t deviceAddress,
                                uint32_t subAddress,
                                uint8_t subAddressSize,
                                uint8_t *rxBuff,
                                uint8_t rxBuffSize)
{
    lpi2c_master_transfer_t masterXfer;

    /* Prepare transfer structure. */
    masterXfer.slaveAddress = deviceAddress;
    masterXfer.direction = kLPI2C_Read;
    masterXfer.subaddress = subAddress;
    masterXfer.subaddressSize = subAddressSize;
    masterXfer.data = rxBuff;
    masterXfer.dataSize = rxBuffSize;
    masterXfer.flags = kLPI2C_TransferDefaultFlag;

    /* Calling I2C Transfer API to start receive. */
    return LPI2C_RTOS_Transfer(handle, &masterXfer);
}

static status_t Codec_I2C_SendFunc(
    uint8_t deviceAddress, uint32_t subAddress, uint8_t subAddressSize, const uint8_t *txBuff, uint8_t txBuffSize)
{
    /* Calling I2C Transfer API to start send. */
    return I2C_SendFunc(codecI2cHandle, deviceAddress, subAddress, subAddressSize, txBuff, txBuffSize);
}

static status_t Codec_I2C_ReceiveFunc(
    uint8_t deviceAddress, uint32_t subAddress, uint8_t subAddressSize, uint8_t *rxBuff, uint8_t rxBuffSize)
{
    /* Calling I2C Transfer API to start receive. */
    return I2C_ReceiveFunc(codecI2cHandle, deviceAddress, subAddress, subAddressSize, rxBuff, rxBuffSize);
}

static void APP_SRTM_PollLinkup(srtm_dispatcher_t dispatcher, void *param1, void *param2)
{
    if (srtmState == APP_SRTM_StateRun)
    {
        if (rpmsg_lite_is_link_up(rpmsgHandle))
        {
            srtmState = APP_SRTM_StateLinkedUp;
            xSemaphoreGive(monSig);
        }
        else
        {
            /* Start timer to poll linkup status. */
            xTimerStart(linkupTimer, portMAX_DELAY);
        }
    }
}

static void APP_LinkupTimerCallback(TimerHandle_t xTimer)
{
    srtm_procedure_t proc = SRTM_Procedure_Create(APP_SRTM_PollLinkup, NULL, NULL);

    if (proc)
    {
        SRTM_Dispatcher_PostProc(disp, proc);
    }
}

static void APP_SRTM_NotifyPeerCoreReady(struct rpmsg_lite_instance *rpmsgHandle, bool ready)
{
    if (rpmsgMonitor)
    {
        rpmsgMonitor(rpmsgHandle, ready, rpmsgMonitorParam);
    }
}

static void APP_SRTM_Linkup(void)
{
    srtm_channel_t chan;
    srtm_rpmsg_endpoint_config_t rpmsgConfig;

    APP_SRTM_NotifyPeerCoreReady(rpmsgHandle, true);

    /* Create SRTM peer core */
    core = SRTM_PeerCore_Create(PEER_CORE_ID);

    /* Set peer core state to activated */
    SRTM_PeerCore_SetState(core, SRTM_PeerCore_State_Activated);

    /* Common RPMsg channel config */
    rpmsgConfig.localAddr = RL_ADDR_ANY;
    rpmsgConfig.peerAddr = RL_ADDR_ANY;
    rpmsgConfig.rpmsgHandle = rpmsgHandle;

    /* Create and add SRTM AUTO channel to peer core */
    rpmsgConfig.epName = APP_SRTM_AUTO_CHANNEL_NAME;
    chan = SRTM_RPMsgEndpoint_Create(&rpmsgConfig);
    SRTM_PeerCore_AddChannel(core, chan);

    /* Create and add SRTM Audio channel to peer core */
    rpmsgConfig.epName = APP_SRTM_AUDIO_CHANNEL_NAME;
    chan = SRTM_RPMsgEndpoint_Create(&rpmsgConfig);
    SRTM_PeerCore_AddChannel(core, chan);

    SRTM_Dispatcher_AddPeerCore(disp, core);
}

static void APP_SRTM_InitPeerCore(void)
{
    rpmsgHandle = rpmsg_lite_remote_init((void *)RPMSG_LITE_SRTM_SHMEM_BASE, RPMSG_LITE_SRTM_LINK_ID, RL_NO_FLAGS);
    assert(rpmsgHandle);

    if (rpmsg_lite_is_link_up(rpmsgHandle))
    {
        /* If resume context has already linked up, don't need to announce channel again. */
        APP_SRTM_Linkup();
    }
    else
    {
        /* Start timer to poll linkup status. */
        xTimerStart(linkupTimer, portMAX_DELAY);
    }
}

static srtm_status_t APP_SRTM_Auto_RegisterEvent(srtm_auto_adapter_t adapter,
                                                 uint32_t clientId,
                                                 uint8_t *reqData,
                                                 uint32_t reqLen,
                                                 uint8_t *respData,
                                                 uint32_t respLen)
{
    srtm_status_t status = SRTM_Status_Success;

    if (appRegHandler)
    {
        status = appRegHandler(clientId, true, appRegParam, reqData, reqLen, respData, respLen) ? SRTM_Status_Success :
                                                                                                  SRTM_Status_Error;
    }

    return status;
}

static srtm_status_t APP_SRTM_Auto_UnregisterEvent(srtm_auto_adapter_t adapter,
                                                   uint32_t clientId,
                                                   uint8_t *reqData,
                                                   uint32_t reqLen,
                                                   uint8_t *respData,
                                                   uint32_t respLen)
{
    srtm_status_t status = SRTM_Status_Success;

    if (appRegHandler)
    {
        status = appRegHandler(clientId, false, appRegParam, reqData, reqLen, respData, respLen) ? SRTM_Status_Success :
                                                                                                   SRTM_Status_Error;
    }

    return status;
}

static srtm_status_t APP_SRTM_Auto_Control(srtm_auto_adapter_t adapter,
                                           uint32_t clientId,
                                           uint8_t *reqData,
                                           uint32_t reqLen,
                                           uint8_t *respData,
                                           uint32_t respLen)
{
    /* Not implemented. */
    return SRTM_Status_ServiceNotFound;
}

static srtm_status_t APP_SRTM_Auto_PowerReport(srtm_auto_adapter_t adapter,
                                               uint32_t clientId,
                                               uint8_t *reqData,
                                               uint32_t reqLen,
                                               uint8_t *respData,
                                               uint32_t respLen)
{
    /* Not implemented. */
    return SRTM_Status_ServiceNotFound;
}

static srtm_status_t APP_SRTM_Auto_GetInfo(srtm_auto_adapter_t adapter,
                                           uint32_t clientId,
                                           uint8_t *reqData,
                                           uint32_t reqLen,
                                           uint8_t *respData,
                                           uint32_t respLen)
{
    /* Not implemented. */
    return SRTM_Status_ServiceNotFound;
}

static void APP_SRTM_InitAutoService(void)
{
    /* Create and register Sensor service */
    autoService = SRTM_AutoService_Create(&autoAdapter);
    SRTM_Dispatcher_RegisterService(disp, autoService);
}

static void APP_SRTM_InitI2C(lpi2c_rtos_handle_t *handle, LPI2C_Type *base, uint32_t baudrate, uint32_t clockrate)
{
    status_t status;
    lpi2c_master_config_t lpi2cConfig;

    /* Initialize LPI2C instance for PMIC and sensor */
    /*
     * lpi2cConfig.debugEnable = false;
     * lpi2cConfig.ignoreAck = false;
     * lpi2cConfig.pinConfig = kLPI2C_2PinOpenDrain;
     * lpi2cConfig.baudRate_Hz = 100000U;
     * lpi2cConfig.busIdleTimeout_ns = 0;
     * lpi2cConfig.pinLowTimeout_ns = 0;
     * lpi2cConfig.sdaGlitchFilterWidth_ns = 0;
     * lpi2cConfig.sclGlitchFilterWidth_ns = 0;
     */
    LPI2C_MasterGetDefaultConfig(&lpi2cConfig);
    lpi2cConfig.baudRate_Hz = baudrate;
    /* Initialize LPI2C RTOS driver. */
    status = LPI2C_RTOS_Init(handle, base, &lpi2cConfig, clockrate);
    if (status != kStatus_Success)
    {
        assert(false);
    }
}

static void APP_SRTM_InitAudioDevice(void)
{
    APP_SRTM_InitI2C(&lpi2cHandle, EXAMPLE_LPI2C, EXAMPLE_LPI2C_BAUDRATE, I2C_SOURCE_CLOCK_FREQ);
    codecI2cHandle = &lpi2cHandle;
    NVIC_SetPriority(M4_LPI2C_IRQn, APP_LPI2C_IRQ_PRIO);
}

static status_t APP_SRTM_ReadCodecRegMap_wm8960(void *handle, uint32_t reg, uint32_t *val)
{
    assert(false);
    return SRTM_Status_Error;
}

static status_t APP_SRTM_WriteCodecRegMap_wm8960(void *handle, uint32_t reg, uint32_t val)
{
    return WM8960_WriteReg((codec_handle_t *)handle, reg, val);
}

static void APP_SRTM_InitAudioService(void)
{
    static srtm_i2c_codec_config_t i2cCodecConfig_wm8960, i2cCodecConfig_cs42888;
    static srtm_codec_adapter_t codecAdapter_wm8960, codecAdapter_cs42888;
    static codec_handle_t wm8960Handle;
    static codec_handle_t cs42888Handle;

    /*
     * For 8QX, the only needed RPC API is codec->getReg and codec->setReg, and
     * the codec initialization is completely handled by A core, so no need to
     * provide the op interface like Init/Deinit/SetFormat, also the codec cached
     * register read/write is not needed here. The codec adapter just service as
     * an low level I2C read/write API
     */
    APP_SRTM_InitAudioDevice();

    i2cCodecConfig_wm8960.addrType = kCODEC_RegAddr8Bit;
    i2cCodecConfig_wm8960.regWidth = kCODEC_RegWidth8Bit;
    i2cCodecConfig_wm8960.readRegMap = APP_SRTM_ReadCodecRegMap_wm8960;
    i2cCodecConfig_wm8960.writeRegMap = APP_SRTM_WriteCodecRegMap_wm8960;

    /*
     * WM8960
     */
    wm8960Handle.I2C_SendFunc = Codec_I2C_SendFunc;
    wm8960Handle.I2C_ReceiveFunc = Codec_I2C_ReceiveFunc;
    wm8960Handle.slaveAddress = WM8960_I2C_ADDR;
    codecAdapter_wm8960 = SRTM_I2CCodecAdapter_Create(&wm8960Handle, &i2cCodecConfig_wm8960);

    /*
     * CS42888
     */
    i2cCodecConfig_cs42888.addrType = kCODEC_RegAddr8Bit;
    i2cCodecConfig_cs42888.regWidth = kCODEC_RegWidth8Bit;
    i2cCodecConfig_cs42888.readRegMap = NULL;
    i2cCodecConfig_cs42888.writeRegMap = NULL;
    cs42888Handle.I2C_SendFunc = Codec_I2C_SendFunc;
    cs42888Handle.I2C_ReceiveFunc = Codec_I2C_ReceiveFunc;
    cs42888Handle.slaveAddress = CS42888_I2C_ADDR;
    codecAdapter_cs42888 = SRTM_I2CCodecAdapter_Create(&cs42888Handle, &i2cCodecConfig_cs42888);

    /* Create and register audio service with 2 interfaces */
    audioService = SRTM_AudioService_Create(NULL, NULL);
    SRTM_AudioService_SetAudioInterface(audioService, 1, NULL, codecAdapter_wm8960);
    SRTM_AudioService_SetAudioInterface(audioService, 2, NULL, codecAdapter_cs42888);

    SRTM_Dispatcher_RegisterService(disp, audioService);
}

static void APP_SRTM_InitServices(void)
{
    APP_SRTM_InitAutoService();
    APP_SRTM_InitAudioService();
}

static void SRTM_MonitorTask(void *pvParameters)
{
    app_srtm_state_t state = APP_SRTM_StateShutdown;

    /* Initialize services and add to dispatcher */
    APP_SRTM_InitServices();

    /* Start SRTM dispatcher */
    SRTM_Dispatcher_Start(disp);

    /* automatically kick off the state machine*/
    // APP_SRTM_StartCommunication();

    /* Monitor peer core state change */
    while (true)
    {
        xSemaphoreTake(monSig, portMAX_DELAY);

        if (state == srtmState)
        {
            continue;
        }

        switch (srtmState)
        {
            case APP_SRTM_StateRun:
                assert(state == APP_SRTM_StateShutdown);
                SRTM_Dispatcher_Stop(disp);
                APP_SRTM_InitPeerCore();
                SRTM_Dispatcher_Start(disp);
                state = APP_SRTM_StateRun;
                break;

            case APP_SRTM_StateLinkedUp:
                if (state == APP_SRTM_StateRun)
                {
                    SRTM_Dispatcher_Stop(disp);
                    APP_SRTM_Linkup();
                    SRTM_Dispatcher_Start(disp);
                }
                break;

            default:
                assert(false);
                break;
        }
    }
}

static void SRTM_DispatcherTask(void *pvParameters)
{
    SRTM_Dispatcher_Run(disp);
}

static void APP_SRTM_InitPeriph(bool resume)
{
}

void APP_SRTM_Init(void)
{
    APP_SRTM_InitPeriph(false);

    monSig = xSemaphoreCreateBinary();
    assert(monSig);

    linkupTimer =
        xTimerCreate("Linkup", APP_MS2TICK(APP_LINKUP_TIMER_PERIOD_MS), pdFALSE, NULL, APP_LinkupTimerCallback);
    assert(linkupTimer);

    /* Create SRTM dispatcher */
    disp = SRTM_Dispatcher_Create();

    xTaskCreate(SRTM_MonitorTask, "SRTM monitor", 256U, NULL, APP_SRTM_MONITOR_TASK_PRIO, NULL);
    xTaskCreate(SRTM_DispatcherTask, "SRTM dispatcher", 512U, NULL, APP_SRTM_DISPATCHER_TASK_PRIO, NULL);
}

void APP_SRTM_StartCommunication(void)
{
    srtmState = APP_SRTM_StateRun;
    xSemaphoreGive(monSig);
}

void APP_SRTM_SetAutoRegisterHandler(app_auto_register_handler_t handler, void *param)
{
    appRegHandler = handler;
    appRegParam = param;
}

bool APP_SRTM_SendAutoCommand(uint32_t clientId,
                              srtm_auto_cmd_t cmd,
                              uint8_t *cmdParam,
                              uint32_t paramLen,
                              uint8_t *result,
                              uint32_t resultLen,
                              uint32_t timeout)
{
    assert(autoService);

    if (SRTM_AutoService_SendCommand(autoService, clientId, cmd, cmdParam, paramLen, result, resultLen, timeout) ==
        SRTM_Status_Success)
    {
        return true;
    }

    return false;
}

void APP_SRTM_Suspend(void)
{
}

void APP_SRTM_Resume(bool resume)
{
    if (resume)
    {
    }
}

void APP_SRTM_SetRpmsgMonitor(app_rpmsg_monitor_t monitor, void *param)
{
    rpmsgMonitor = monitor;
    rpmsgMonitorParam = param;
}
