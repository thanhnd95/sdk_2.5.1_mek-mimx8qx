Release Name: MCUXpresso Software Development Kit (SDK)
Release Version: 2.5.1

Amazon FreeRTOS            Author: Real Time Engineers, Ltd.                 
                           Description: IoT operating system for             
                           microcontrollers                                  
                           Format: source code                               
                           License: MIT. See ,<rootfolder>/rtos/amazon-      
                           freertos/License.                                 
                           Location: rtos/amazon-freertos                    
                           name: Amazon FreeRTOS                             
                           URL: https://aws.amazon.com/freertos/             
                           version: 1.4.0                                    

CMSIS Core header files    Author: ARM                                       
                           parentversion: 5.0.1                              
                           URL: http://silver.arm.com                        
                           name: CMSIS Core header files                     
                           Format: source code                               
                           License: Apache License 2.0, See                  
                           <rootfolder>/CMSIS/LICENSE.txt                    
                           Location: CMSIS/Include                           
                           version: 5.0.1 (CMSIS Download 5.0.1)             
                           Description: Industry-standard header files       
                           distributes by ARM for cortex-M cores             

CMSIS DSP Library          name: CMSIS DSP Library                           
                           Format: source code                               
                           License: Apache License 2.0, See                  
                           <rootfolder>/CMSIS/LICENSE.txt                    
                           parentversion: 5.0.1                              
                           Author: ARM                                       
                           version: 5.0.1 (CMSIS download 5.0.1)             
                           URL: http://silver.arm.com                        
                           Location: CMSIS/DSP_Lib                           
                           Description: A digital signal processing library  
                           for Cortex-M cores from ARM                       

KSDK Peripheral Driver     Description: Peripheral drivers are designed for  
                           the most common use cases indentified for         
                           the underlying hardware block.                    
                           Author: Freescale                                 
                           Version: 2.x.x                                    
                           License: Open Source - BSD-3-Clause               
                           Format: source code                               
                           Location: devices/<device_name>/drivers           

RPMsg-Lite                 version: 2.0.1                                    
                           Location: middleware/multicore/rpmsg_lite/        
                           Description: Open Asymmetric Multi Processing     
                           (OpenAMP) framework project                       
                           License: Open Source - BSD-3-Clause               
                           Author: Mentor Graphics Corporation & community   
                           contributors                                      
                           name: RPMsg-Lite                                  
                           Format: source code                               
                           URL: https://github.com/OpenAMP/open-amp          

lwIP TCP/IP Stack          Author: Swedish Institute of Computer Science     
                           Description: A light-weight TCP/IP stack          
                           Format: source code                               
                           License: Open Source - BSD-3-Clause               
                           Location: middleware/lwip                         
                           name: lwIP TCP/IP Stack                           
                           URL: http://savannah.nongnu.org/projects/lwip     
                           version: 2.0.3                                    

mmCAU S/W Library          Author: Freescale                                 
                           Description: S/W library that works with the      
                           memory-mapped cryptographic acceleration unit     
                           present on some MCUXpresso SoCs                   
                           Format: source code                               
                           License: Open Source - BSD-3-Clause               
                           Location: middleware/mmcau                        
                           name: mmCAU S/W Library                           
                           version: 2.0.1                                    
